package com.yml.newinfoindia;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class DthFragment extends Fragment{
	Button valueButton;
	Spinner operatorSpinner ;
	Spinner circleSpinner;
	ListView listview;
	public DthFragment() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//return super.onCreateView(inflater, container, savedInstanceState);
		View rootView = inflater.inflate(R.layout.dth_list, container, false);
		operatorSpinner=(Spinner)rootView.findViewById(R.id.operatorSpinner);
		circleSpinner = (Spinner)rootView.findViewById(R.id.circleSpinner);
		listview=(ListView)rootView.findViewById(R.id.listView1);
		valueButton = (Button)rootView.findViewById(R.id.submit);
		final	Context mContext;
			mContext=rootView.getContext();
			valueButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String OPERATOR = operatorSpinner.getSelectedItem().toString();
					String LOCATION = circleSpinner.getSelectedItem().toString();
					Log.d("DTH", "DTH_Data");
					Log.d("Spinner Value","item0"+OPERATOR);
					
					String dthApi="http://api.dataweave.in/v1/dth_data/listByCircle/?api_key=b20a79e582ee4953ceccf41ac28aa08d&operator="+OPERATOR+"&circle="+LOCATION+"&page=1&per_page=50";
					dislpayList(dthApi);
					
					
					
					
					
				}
				
				private void dislpayList(String Api) {
					
			//int check=1;
					
					new DownloadParserClass(mContext, listview).execute(Api);
					//new ParseClass(mContext, listview, check).execute(Api);
					}
			});
			 return rootView;
	}

}
