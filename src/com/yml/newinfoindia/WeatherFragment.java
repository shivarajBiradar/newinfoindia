package com.yml.newinfoindia;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class WeatherFragment extends Fragment{
	Button valueButton;
	Spinner citySpinner ;
	Spinner dateSpinner;
	ListView listview;
	public WeatherFragment() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootView = inflater.inflate(R.layout.weather_list, container, false);
		citySpinner=(Spinner)rootView.findViewById(R.id.operatorSpinner);
		dateSpinner = (Spinner)rootView.findViewById(R.id.circleSpinner);
		listview=(ListView)rootView.findViewById(R.id.listView1);
		
		valueButton = (Button)rootView.findViewById(R.id.submit);
	final	Context mContext;
		mContext=rootView.getContext();
		
		valueButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String LOCATION = citySpinner.getSelectedItem().toString();
				String DATE = dateSpinner.getSelectedItem().toString();
				Log.d("Weather", "Weatherdata");
				Log.d("Spinner Value","item0"+LOCATION);
				String weatherApi="http://api.dataweave.in/v1/indian_weather/findByCity/?api_key=b20a79e582ee4953ceccf41ac28aa08d&city="+LOCATION+"&date="+DATE+"";
				
				dislpayList(weatherApi);
				
			}
			private void dislpayList(String Api) {
				

				
				new WeatherParseClass(mContext, listview).execute(Api);
				}
		});
		 return rootView;
	
	}

}
