package com.yml.newinfoindia;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

public class WeatherParseClass extends AsyncTask<String, String, ArrayList<WeatherClass>> {

	Context context;
	ArrayList<WeatherClass> weatherClass;
	ListView listView;
	private ProgressDialog pDialog;

	public WeatherParseClass(Context context, ListView listView) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.listView = listView;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please Wait");
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@Override
	protected ArrayList<WeatherClass> doInBackground(String... finalURL) {
		// TODO Auto-generated method stub
		
		//passing url to handle the url request
		
		String link = finalURL[0];
		ServiceHandler serviceHandler = new ServiceHandler();
		String jsonStr = serviceHandler.makeHttpRequest(link);

		Log.d("Response: ", "> " + jsonStr);

		if (jsonStr != null) {
			try {

				Log.d("DOing in Background", "FINISHED url");

				JSONObject readerObject = new JSONObject(jsonStr);

				JSONArray dataArray = readerObject.getJSONArray("data");

				JSONObject dataObject = dataArray.getJSONObject(0);

				WeatherClass weatherClassObj;
				weatherClass = new ArrayList<WeatherClass>();
				JSONArray MinimumArray = dataObject
						.getJSONArray("Minimum Temp");

				String Minimum = MinimumArray.getString(0)
						+ MinimumArray.getString(1);

				String date = dataObject.getString("date");

				Log.d("DOing in Background", "FINISHED url");

				JSONArray maximumArray = dataObject
						.getJSONArray("Maximum Temp");

				String maximum = maximumArray.getString(0)
						+ maximumArray.getString(1);

				JSONArray moonsetArray = dataObject.getJSONArray("Moonset");
				String moonset = moonsetArray.getString(0)
						+ moonsetArray.getString(1);

				JSONArray sunsetArray = dataObject
						.getJSONArray("Todays Sunset");
				String sunset = sunsetArray.getString(0)
						+ sunsetArray.getString(1);
				weatherClassObj = new WeatherClass(Minimum, date, maximum,
						moonset, sunset);

				weatherClass.add(weatherClassObj);

				System.out.println(weatherClass.toString());

				Log.d("onEXECUTE", "HI there");

			} catch (JSONException e) {
				return null;
			} catch (Exception e) {
				return null;
			}
		}
		return weatherClass;

	}

	@Override
	protected void onPostExecute(ArrayList<WeatherClass> result) {
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}

		// TODO Auto-generated method stub
		try {

			Log.d("onPostEXECUTE", "seems to be done with parsing");

			WeatherAdapterClass adapter = new WeatherAdapterClass(context,
					result);

			listView.setAdapter(adapter);

			// is.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
