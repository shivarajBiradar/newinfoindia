package com.yml.newinfoindia;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

public class CarParseClass extends
		AsyncTask<String, String, ArrayList<CarClass>> {
	Context context;

	ArrayList<CarClass> carClass;
	ListView listView;

	private ProgressDialog pDialog;

	public CarParseClass(Context cotext, ListView listView) {
		// TODO Auto-generated constructor stub
		this.context = cotext;
		this.listView = listView;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();
	}

	@Override
	protected ArrayList<CarClass> doInBackground(String... finalURL) {
		// TODO Auto-generated method stub

		String link = finalURL[0];
		ServiceHandler serviceHandler = new ServiceHandler();
		String jsonStr = serviceHandler.makeHttpRequest(link);

		Log.d("Response: ", "> " + jsonStr);

		if (jsonStr != null) {
			try {

				Log.d("DOing in Background", "FINISHED url");

				JSONObject readerObject = new JSONObject(jsonStr);

				JSONArray dataArray = readerObject.getJSONArray("data");

				String ss = dataArray.toString();
				Log.d("recharge_value", ss);
				CarClass carClassObj;
				carClass = new ArrayList<CarClass>();

				for (int i = 0; i < 10; i++) {

					JSONObject jsonObj = dataArray.getJSONObject(i);

					Log.d("DOing in Background", "FINISHED url");
					String model = jsonObj.getString("model");
					String fuel_type = jsonObj.getString("fuel_type");
					String price = jsonObj.getString("price");
					String state = jsonObj.getString("state");
					String crawl_date = jsonObj.getString("crawl_date");

					carClassObj = new CarClass(model, fuel_type, price, state,
							crawl_date);
					carClass.add(carClassObj);

					System.out.println(carClass.toString());

				}
				Log.d("onEXECUTE", "HI there");

			} catch (Exception e) {
				return null;
			}
		}
		return carClass;

	}

	@Override
	protected void onPostExecute(ArrayList<CarClass> result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}

		try {

			Log.d("onPostEXECUTE", "seems to be done with parsing");

			Log.d("onPostEXECUTE", "HI" + result.toString());

			CarAdapterClass adapter = new CarAdapterClass(context, result);

			listView.setAdapter(adapter);

	

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
