package com.yml.newinfoindia;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class CarFragment extends Fragment{
	Button valueButton;
	Spinner carSpinner ;
	Spinner locationSpinner;
	ListView listview;
	public CarFragment() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.car_list, container, false);
		carSpinner=(Spinner)rootView.findViewById(R.id.operatorSpinner);
		locationSpinner = (Spinner)rootView.findViewById(R.id.circleSpinner);
		listview=(ListView)rootView.findViewById(R.id.listView1);
		
		valueButton = (Button)rootView.findViewById(R.id.submit);
		final	Context mContext;
		mContext=rootView.getContext();
		valueButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String MAKECAR = carSpinner.getSelectedItem().toString();
				String CARCITY = locationSpinner.getSelectedItem().toString();
				Log.d("car", "CarData");
				Log.d("Spinner Value","item0"+MAKECAR);
				String carApi="http://api.dataweave.in/v1/carPricesIndia/findByMake/?api_key=b20a79e582ee4953ceccf41ac28aa08d&make="+MAKECAR+"&city="+CARCITY+"&page=1&per_page=50";
				
				dislpayList(carApi);
				
			}
			private void dislpayList(String carApi) {
				
	//int value=1;
				
				new CarParseClass(mContext, listview).execute(carApi);
			//new ParseClass(mContext, listview, value).onPostExecute(carApi);
				}
		});
		 return rootView;
	
	}

}
